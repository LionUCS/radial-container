tool
extends EditorPlugin


func _enter_tree():
	add_custom_type("RadialContainer", "Container", preload("res://addons/radial_container/radial_container.gd"), preload("res://addons/radial_container/icon.png"))

func _exit_tree():
	remove_custom_type("RadialContainer")
