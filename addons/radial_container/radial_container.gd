tool
extends Container

export var phase_offset : float = 0.0 setget set_phase_offset

func _ready():
	connect("sort_children", self, "place_children")

func place_children():
	var center = self.rect_size/2
	var children = get_children()
	if children.size() == 0:
		return
	var angle_offset = TAU/children.size()
	var angle = TAU/4+phase_offset
	for c in children:
		var x = cos(angle)*center.x+center.x
		var y = sin(angle)*center.y-center.y
		var corner_pos = Vector2(x, -y)-(c.get_size()/2)
		c.set_position(corner_pos)
		angle += angle_offset

func set_phase_offset(value):
	phase_offset = value
	place_children()
