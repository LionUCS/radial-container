# Radial Container

An elliptical container for the Godot game engine

based on Godot [Q&A answer from user DomtronVox](https://godotengine.org/qa/32497/how-to-a-radial-menu-pie-menu?show=43149#a43149)


# Download & Install
Download the addon from https://gitlab.com/LionuCS/radial-container.

Copy the `addons` folder in your project directory.

In `Project/Project Settings/Plugins` enable *RadialContainer*.

# Usage
Radial Container works the same as Box Containers. Child nodes will position in an ellipse of the container bounding box
